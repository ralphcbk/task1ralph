from flask import Flask, render_template, url_for, flash, redirect, session
from forms import RegistrationForm, LoginForm, CreateAssetForm
from flask_pymongo import PyMongo

app = Flask(__name__)

app.config['SECRET_KEY'] = 'da8dc9a5e6ddef9fd0c28740ddea62bd'
app.config['MONGO_DBNAME'] = 'AMS'
app.config["MONGO_URI"] = "mongodb://localhost:27017/AMS"
mongo = PyMongo(app)


@app.route("/")
def home():
    asset = mongo.db.assets
    user = mongo.db.users
    assetlist = asset.find()
    assetlist2 = asset.find()
    if 'username' in session and user.find_one({'username': session['username'], 'role': 'admin'}):
        return render_template('adminHome.html', assetlist=assetlist, assetlist2=assetlist2)
    elif 'username' in session:
        return render_template('userHome.html', assetlist=assetlist)
    return redirect(url_for('login'))


@app.route("/signout")
def signout():
    session.clear()
    return redirect(url_for('login'))


@app.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        user = mongo.db.users
        existing = user.find_one({'username': form.username.data})
        if existing:
            flash(f'{form.name.data} already exists!', 'danger')
            return render_template('registration.html', title='Register', form=form)
        a = user.insert_one({'username': form.username.data,
                             'name': form.name.data, 'password': form.password.data, 'role': 'user'})
        session['username'] = form.username.data
        flash(f'Account registered for {form.name.data}!', 'success')
        return redirect(url_for('home'))
    return render_template('registration.html', title='Register', form=form)


@app.route("/login", methods=['GET', 'POST'])
def login():
    form = LoginForm()
    if form.validate_on_submit():
        user = mongo.db.users
        existing = user.find_one(
            {'username': form.username.data, 'password': form.password.data})
        if existing:
            flash(f'Successful Login for {form.username.data}', 'success')
            session['username'] = form.username.data
            session['role'] = existing['role']
            return redirect(url_for('home'))
        flash('Username or password is incorrect', 'danger')
    return render_template('login.html', title='Login', form=LoginForm())


@app.route("/createAsset", methods=['GET', 'POST'])
def addAsset():
    form = CreateAssetForm()
    if form.validate_on_submit():
        asset = mongo.db.assets
        existing = asset.find_one({'name': form.name.data})
        if existing:
            flash(f'{form.name.data} already exists!', 'danger')
            return render_template('createAsset.html', title='Create Asset', form=form)
        asset.insert_one(
            {'name': form.name.data, 'status': "Free", 'borrowedBy': ""})
        flash(f'{form.name.data} created!', 'success')
        return redirect(url_for('home'))
    return render_template('createAsset.html', title='Create Asset', form=form)


@app.route("/find", methods=['GET', 'POST'])
def find():
    user = mongo.db.users
    ralph = user.find_one({'username': 'ralphcbk'})
    return ralph['name'] + " and his role is " + ralph['role'] + " and object id is " +str(ralph['_id'])

@app.route("/asset/<asset_id>", methods=['GET', 'POST'])
def assetspecific(asset_id):
    asset = mongo.db.assets
    assetid = asset.find_one({'name':asset_id})
    return render_template('assetSpecific.html', title=assetid['name'], assetid=assetid)

if __name__ == '__main__':
    app.run(debug=True)
